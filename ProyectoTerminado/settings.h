#ifndef SETTINGS_H
#define SETTINGS_H

#include <QWidget>
#include "widget.h"
#include "simulator.h"
class Simulator;
namespace Ui {
class settings;
}

class settings : public QWidget
{
    Q_OBJECT

public:
    explicit settings(QWidget *parent = nullptr);
    ~settings();
    void set_Things(Widget *window = NULL, Simulator *sim = NULL);

private:
    Ui::settings *ui;
    Widget *window;
    Simulator *sim;
private slots:
    void on_back();
    void on_save();
};

#endif // SETTINGS_H
