#include "widget.h"
#include "simulator.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    w.show();
    Simulator *sim = new Simulator();
    w.set_Things(sim);
    return a.exec();
}
