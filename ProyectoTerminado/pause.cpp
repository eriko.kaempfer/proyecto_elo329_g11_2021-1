#include "pause.h"
#include "ui_pause.h"
#include <iostream>

using namespace std;
Pause::Pause(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Pause),
    window(NULL),
    mainMenu(NULL),
    sim(NULL)
{
    ui->setupUi(this);
    connect(ui->Continue,SIGNAL(clicked()),this,SLOT(on_continue()));
    connect(ui->Mm,SIGNAL(clicked()),this,SLOT(on_menu()));
}

Pause::~Pause()
{
    delete ui;
}
void Pause::set_Things(Game *window,Widget *mainMenu, Simulator *sim){
    this->window = window;
    this->mainMenu = mainMenu;
    this->sim = sim;
}
void Pause::on_continue(){
    sim->continueTimer();
    window->setVisible(1);
    this->close();
}
void Pause::on_menu(){
    mainMenu->setVisible(1);
    window->close();
    this->close();
}
