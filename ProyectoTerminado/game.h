#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include "widget.h"
#include <QKeyEvent>
#include <iostream>
#include <QGraphicsScene>
#include "pause.h"
#include <QLabel>
#include <QVector>
#include <vector>
#include <QRandomGenerator>
//#include "simulator.h"
using namespace std;
class Simulator;
namespace Ui {
//class Simulator;
class Game;

}

class Game : public QWidget
{
    Q_OBJECT

public:
    explicit Game(QWidget *parent = nullptr);
    ~Game();
    void set_Things(Widget *window = NULL,Simulator *sim = NULL);
    void keyPressEvent(QKeyEvent *event);
    void crear_bala();
    friend class QLabel;
    vector <QLabel*> *enemigos;
    vector <QLabel*> *balas;
    vector <QLabel*> *balas_enemigos;
    QLabel *DT;
    QRandomGenerator myRand;
    void set_lifes(int l);
    void set_score(int s);
    void set_diff(int d);
    Widget *window = NULL;

private:
    Ui::Game *ui;
    int pos_actual_x = 360;
    int pos_actual_y = 500;
    Simulator *sim;
private slots:
    void on_pause();
};

#endif // GAME_H
