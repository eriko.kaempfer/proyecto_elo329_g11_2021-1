#include "simulator.h"

Simulator::Simulator(QObject *parent) : QObject(parent)
{
    timer = new QTimer(this);
    timer_balas = new QTimer(this);
    timer_balas_enemigos = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this, SLOT(simulateSlot()));
    connect(timer_balas,SIGNAL(timeout()),this,SLOT(simulateSlot_balas()));
    connect(timer_balas_enemigos,SIGNAL(timeout()),this,SLOT(simulateSlot_balas_enemigos()));
    connect(timer_balas,SIGNAL(timeout()),this,SLOT(simulateSlot_move_balas()));
}

void Simulator::startSimulation(){
    desp = 10;
    inicio = 1150;
    this->game->set_diff(diff);
    lifes = initial_lifes;
    this->game->set_lifes(initial_lifes);
    timer->start(inicio-150*diff);
    timer_balas->start(20);
    timer_balas_enemigos->start(1000-150*diff);
}

void Simulator::simulateSlot(){
    int pos_x;
    int pos_y;
    for(int i = 0;i<(int)this->game->enemigos->size();i++){
       pos_x = this->game->enemigos->at(i)->pos().x();
       pos_y = this->game->enemigos->at(i)->pos().y();
       this->game->enemigos->at(i)->setGeometry(pos_x+desp,pos_y,55,30);

    }
    for(int i = 0; i<(int)this->game->enemigos->size(); i++){
        if (this->game->enemigos->at(i)->pos().x()+10>=720){
            desp = -10;
            for(int j = 0;j<(int)this->game->enemigos->size();j++){
                pos_x = this->game->enemigos->at(j)->pos().x();
                pos_y = this->game->enemigos->at(j)->pos().y();
                this->game->enemigos->at(j)->setGeometry(pos_x,pos_y+8,55,30);
            }
        }
       if (this->game->enemigos->at(i)->pos().x()-10<=30){
           desp = 10;
           for(int j = 0;j<(int)this->game->enemigos->size();j++){
               pos_x = this->game->enemigos->at(j)->pos().x();
               pos_y = this->game->enemigos->at(j)->pos().y();
               this->game->enemigos->at(j)->setGeometry(pos_x,pos_y+8,55,30);
           }
       }
       if (this->game->enemigos->at(i)->pos().y()+10>=470){
           timer->stop();
       }
    }
}

void Simulator::simulateSlot_balas(){
    int pos_x_bala;
    int pos_y_bala;
    int pos_x_enemigo;
    int pos_y_enemigo;
    int w = 80;
    int h = 60;
    for(int i = 0;i<(int)this->game->balas->size();i++){
        pos_x_bala = this->game->balas->at(i)->pos().x();
        pos_y_bala = this->game->balas->at(i)->pos().y();
        bool cond0 = false;
        if(1){
            for(int j = (int)this->game->enemigos->size()-1;j>=0;j--){
                pos_x_enemigo=this->game->enemigos->at(j)->pos().x();
                pos_y_enemigo=this->game->enemigos->at(j)->pos().y();
                bool cond1 = pos_x_bala<=pos_x_enemigo+w-30;
                bool cond2 = pos_x_bala>=pos_x_enemigo;
                bool cond3 = pos_y_bala<=pos_y_enemigo+h-30;
                bool cond4 = pos_y_bala>=pos_y_enemigo;

                if(cond1 && cond2 && cond3 && cond4){
                    this->game->enemigos->at(j)->close();
                    this->game->enemigos->erase(this->game->enemigos->begin()+j);
                    this->game->balas->at(i)->close();
                    this->game->balas->erase(this->game->balas->begin()+i);
                    if((int)this->game->enemigos->size()>=30){
                        timer->setInterval(800-50*diff);
                        score+=10+5*diff;
                    }
                    else if((int)this->game->enemigos->size()>=20){
                        timer->setInterval(600-50*diff);
                        score+=15+10*diff;
                    }
                    else if((int)this->game->enemigos->size()>=10){
                        timer->setInterval(450-50*diff);
                        score+=25+15*diff;
                    }
                    else if((int)this->game->enemigos->size()>=5){
                        timer->setInterval(200-25*diff);
                        score+=50+25*diff;
                    }
                    else if((int)this->game->enemigos->size()>=4){
                        timer->setInterval(110-10*diff);
                        score+=75+50*diff;
                    }
                    else if((int)this->game->enemigos->size()>=3){
                        timer->setInterval(80-10*diff);
                        score+=100+75*diff;
                    }
                    else if((int)this->game->enemigos->size()>=2){
                        timer->setInterval(60-10*diff);
                        score+=150+100*diff;
                    }
                    else if((int)this->game->enemigos->size()>=1){
                        timer->setInterval(30-5*diff);
                        score+=200+150*diff;
                    }
                    else if((int)this->game->enemigos->size()==0){
                        score+=(1000*diff)/((initial_lifes-lifes+1));
                        this->game->set_score(score);
                        this->set_scores();
                        EndGame *s = new EndGame();
                        s->set_Things(game,game->window,this);
                        pauseTimer();
                        s->show();
                        return;
                    }
                    this->game->set_score(score);
                    cond0 = true;
                    break;
                }
            }
        }
        if(cond0){
            continue;
        }
        else if(this->game->balas->at(i)->pos().y()<=100){
            this->game->balas->at(i)->close();
            this->game->balas->erase(this->game->balas->begin()+i);
        }else{
           this->game->balas->at(i)->setGeometry(pos_x_bala,pos_y_bala-10,10,10);
        }
    }
}

void Simulator::simulateSlot_move_balas(){
    if((int)this->game->balas_enemigos->size()>=1){
        int pos_x_bala;
        int pos_y_bala;
        int pos_x_dt;
        int pos_y_dt;
        int w = 80;
        int h = 60;

        for(int i = 0;i<(int)this->game->balas_enemigos->size();i++){
            pos_x_bala = this->game->balas_enemigos->at(i)->pos().x();
            pos_y_bala = this->game->balas_enemigos->at(i)->pos().y();
            bool cond0 = false;
            if(1){
                pos_x_dt=this->game->DT->pos().x();
                pos_y_dt=this->game->DT->pos().y();

                bool cond1 = pos_x_bala + 10<=pos_x_dt+w;
                bool cond2 = pos_x_bala + 10>=pos_x_dt;
                bool cond3 = pos_y_bala + 10<=pos_y_dt+h-30;
                bool cond4 = pos_y_bala + 10>=pos_y_dt;

                if(cond1 && cond2 && cond3 && cond4){
                    lifes--;
                    if(lifes==0){
                        this->game->set_lifes(lifes);
                        this->set_scores();
                        EndGame *s = new EndGame();
                        s->set_Things(game,game->window,this);
                        pauseTimer();
                        s->show();
                    }else{
                        this->game->set_lifes(lifes);
                        this->game->balas_enemigos->at(i)->close();
                        this->game->balas_enemigos->erase(this->game->balas_enemigos->begin()+i);
                        cond0 = true;
                    }
                }
            }
            if (cond0){
                continue;
            }
            else if(this->game->balas_enemigos->at(i)->pos().y() + 20>=580){
                this->game->balas_enemigos->at(i)->close();
                this->game->balas_enemigos->erase(this->game->balas_enemigos->begin()+i);
            }else if((int)this->game->balas_enemigos->size()>0){
               this->game->balas_enemigos->at(i)->setGeometry(pos_x_bala,pos_y_bala+10,20,20);
            }
        }
    }
}
void Simulator::set_scores(){
    for (int i = 0; i<5;i++ ) {
        if(this->scores[i]<this->score){
            for (int j= 5;j>i ;j-- ) {
                this->scores[j-1]=this->scores[j-2];
            }
            this->scores[i]=this->score;
            break;
        }
    }
}

void Simulator::simulateSlot_balas_enemigos(){
    if((int)this->game->enemigos->size()>0){
        this->game->crear_bala();
    }
}
void Simulator::set_Game(Game *game){
    this->game = game;
    this->game->set_diff(diff);
    this->game->set_lifes(initial_lifes);
    lifes = initial_lifes;
    this->game->set_score(initial_score);
    score = initial_score;
}
void Simulator::pauseTimer(){
    timer->stop();
    timer_balas->stop();
    timer_balas_enemigos->stop();
}
void Simulator::continueTimer(){
    timer->start();
    timer_balas->start();
    timer_balas_enemigos->start();
}
bool Simulator::stateTimer(){
    return timer->isActive();
}

