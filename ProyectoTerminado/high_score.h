#ifndef HIGH_SCORE_H
#define HIGH_SCORE_H

#include <QWidget>
#include "widget.h"
#include <iostream>
#include "simulator.h"
class Simulator;
using namespace std;
namespace Ui {
class High_Score;
}

class High_Score : public QWidget
{
    Q_OBJECT

public:
    explicit High_Score(QWidget *parent = nullptr);
    ~High_Score();
    void set_Things(Widget *window = NULL, Simulator *sim= NULL);
    void set_scores();

private:
    Ui::High_Score *ui;
     Widget *window;
     Simulator *sim;
private slots:
     void on_back();
};

#endif // HIGH_SCORE_H
