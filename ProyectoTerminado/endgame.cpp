#include "endgame.h"
#include "ui_endgame.h"

EndGame::EndGame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EndGame),
    game(NULL),
    mainMenu(NULL),
    sim(NULL)
{
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(set_score()));
}

EndGame::~EndGame()
{
    delete ui;
}

void EndGame ::set_Things(Game *game,Widget *mainMenu, Simulator *sim){
    this->game = game;
    this->mainMenu = mainMenu;
    this->sim = sim;
    ui->Score->setNum(sim->score);
}

void EndGame::set_score(){
    mainMenu->setVisible(1);
    game->close();
    this->close();
}
