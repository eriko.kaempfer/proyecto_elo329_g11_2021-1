#ifndef CONTROLS_H
#define CONTROLS_H

#include <QWidget>
#include "widget.h"
namespace Ui {
class Controls;
}

class Controls : public QWidget
{
    Q_OBJECT

public:
    explicit Controls(QWidget *parent = nullptr);
    ~Controls();
    void set_Things(Widget *window = NULL);

private:
    Ui::Controls *ui;
    Widget *window;
private slots:
     void on_back();
};

#endif // CONTROLS_H
