#include "controls.h"
#include "ui_controls.h"

Controls::Controls(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Controls),
    window(NULL)
{
    ui->setupUi(this);
    connect(ui->BackControl,SIGNAL(clicked()),this,SLOT(on_back()));
}

Controls::~Controls()
{
    delete ui;
}
void Controls::set_Things(Widget *window){
    this->window=window;
}
void Controls::on_back(){
    window->setVisible(1);
    this->close();
}
