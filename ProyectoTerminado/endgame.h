#ifndef ENDGAME_H
#define ENDGAME_H

#include <QWidget>
#include "game.h"
#include "simulator.h"
#include "widget.h"
class Simulator;
class Game;
class Widget;
namespace Ui {
class EndGame;
}

class EndGame : public QWidget
{
    Q_OBJECT

public:
    explicit EndGame(QWidget *parent = nullptr);
    ~EndGame();
    void set_Things(Game *game = NULL, Widget *mainMenu = NULL,Simulator *sim = NULL);

private:
    Ui::EndGame *ui;
    Game *game;
    Widget *mainMenu;
    Simulator *sim;
private slots:
    void set_score();
};

#endif // ENDGAME_H
