#include "settings.h"
#include "ui_settings.h"
#include <iostream>
using namespace std;
settings::settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::settings),
    window(NULL),
    sim(NULL)
{
    ui->setupUi(this);
    connect(ui->Back,SIGNAL(clicked()),this,SLOT(on_back()));
    connect(ui->Save,SIGNAL(clicked()),this,SLOT(on_save()));
}

settings::~settings()
{
    delete ui;
}
void settings::set_Things(Widget *window, Simulator *sim){
    this->sim = sim;
    ui->Lifes->setValue(sim->initial_lifes);
    ui->Dif->setValue(sim->diff);
    this->window=window;
}
void settings::on_back(){
    window->setVisible(1);
    this->close();
}
void settings::on_save(){
    sim->initial_lifes=ui->Lifes->value();
    sim->diff=ui->Dif->value();
    window->setVisible(1);
    this->close();
}
