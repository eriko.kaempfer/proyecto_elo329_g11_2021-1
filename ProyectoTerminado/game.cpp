#include "game.h"
#include "ui_game.h"
#include <QResource>
#include <QThread>
#include <QTimer>
using namespace std;
Game::Game(QWidget *parent) :
    QWidget(parent),
    window(NULL),
    ui(new Ui::Game),
    sim(NULL)
{
    ui->setupUi(this);
    enemigos = new vector <QLabel*>;
    balas = new vector <QLabel*>;
    balas_enemigos = new vector <QLabel*>;

    myRand = QRandomGenerator::securelySeeded();

}
Game::~Game()
{
    delete ui;
}
void Game::set_Things(Widget *window,Simulator *sim){
    this->window=window;
    this->sim = sim;
    this->sim->set_Game(this);
}
void Game::crear_bala(){
    int r = myRand.generateDouble() * enemigos->size();
//    int w = 80;
    int h = 60;
    int w_rayo = 20;
    int h_rayo = 20;
    QString path_rayo = ":/rayo.png";
    QPixmap pix6(path_rayo);
    QLabel *rayo = new QLabel(this);
    rayo->setPixmap(pix6);
    rayo->setScaledContents(1);
    rayo->setStyleSheet("QLabel { background-color: rgba(0,0,0,0%); }");
    int pos_x = enemigos->at(r)->pos().x();
    int pos_y = enemigos->at(r)->pos().y();
    rayo->setGeometry(pos_x+(h/2)-(h_rayo),pos_y+w_rayo,w_rayo,h_rayo);
    balas_enemigos->push_back(rayo);
    rayo->show();
}
void Game::on_pause(){
    cout<<"pausa"<<endl;
}
void Game::keyPressEvent( QKeyEvent * event )
{
    int w = 60;
    int h = 80;
    int w_bala = 10;
    int h_bala = 10;
    if( event->key() == Qt::Key_Escape )
    {
        Pause *s = new Pause();
        s->set_Things(this,window,sim);
        sim->pauseTimer();
        s->show();
        this->setHidden(1);
    }
    if( event->key() == Qt::Key_E )
    {
        if(balas->size()!=0){
                    balas->at(0)->close();
                    balas->erase(balas->begin());
                }
        QString path_bala = ":/bala.png";
        QPixmap pix5(path_bala);
        QLabel *bala = new QLabel(this);
        bala->setPixmap(pix5);
        bala->setScaledContents(1);
        bala->setStyleSheet("QLabel { background-color: rgba(0,0,0,0%); }");
        bala->setGeometry(pos_actual_x+(h/2)-(h_bala),pos_actual_y+w_bala,w_bala,h_bala);
        balas->push_back(bala);
        bala->show();
      }
    if (event->key()==Qt::Key_F){
        if(!sim->stateTimer()){
            QString path_enemigo = ":/enemigo.png";
            QString path_enemigo2 = ":/enemigo2.png";
            QString path_enemigo3 = ":/enemigo3.png";
            QString path_dt = ":/dt.png";
            QPixmap pix(path_dt);
            QPixmap pix2(path_enemigo);
            QPixmap pix3(path_enemigo2);
            QPixmap pix4(path_enemigo3);
            DT = new QLabel(this);
            DT->setPixmap (pix);
            DT->setScaledContents(1);
            DT->setStyleSheet("QLabel { background-color: rgba(0,0,0,0%); }");
            DT->setGeometry(pos_actual_x,pos_actual_y,w,h);
            DT->setFixedSize(w,h);
            DT->show();

            for(int i = 0; i<8;i++){
                QLabel *enemigo = new QLabel(this);
                enemigo->setPixmap(pix2);
                enemigo->setScaledContents(1);
                enemigo->setStyleSheet("QLabel { background-color: rgba(0,0,0,0%); }");
                enemigo->setGeometry(31+i*65+10*i,101,55,30);
                enemigos->push_back(enemigo);
                enemigo->show();
            }
            for(int i = 0; i<8;i++){
                QLabel *enemigo = new QLabel(this);
                enemigo->setPixmap(pix4);
                enemigo->setScaledContents(1);
                enemigo->setStyleSheet("QLabel { background-color: rgba(0,0,0,0%); }");
                enemigo->setGeometry(31+i*65+10*i,136,55,30);
                enemigos->push_back(enemigo);
                enemigo->show();
            }
            for(int i = 0; i<8;i++){
                QLabel *enemigo = new QLabel(this);
                enemigo->setPixmap(pix4);
                enemigo->setScaledContents(1);
                enemigo->setStyleSheet("QLabel { background-color: rgba(0,0,0,0%); }");
                enemigo->setGeometry(31+i*65+10*i,171,55,30);
                enemigos->push_back(enemigo);
                enemigo->show();
            }
            for(int i = 0; i<8;i++){
                QLabel *enemigo = new QLabel(this);
                enemigo->setPixmap(pix3);
                enemigo->setScaledContents(1);
                enemigo->setStyleSheet("QLabel { background-color: rgba(0,0,0,0%); }");
                enemigo->setGeometry(31+i*65+10*i,206,55,30);
                enemigos->push_back(enemigo);
                enemigo->show();
            }
            for(int i = 0; i<8;i++){
                QLabel *enemigo = new QLabel(this);
                enemigo->setPixmap(pix3);
                enemigo->setScaledContents(1);
                enemigo->setStyleSheet("QLabel { background-color: rgba(0,0,0,0%); }");
                enemigo->setGeometry(31+i*65+10*i,241,55,30);
                enemigos->push_back(enemigo);
                enemigo->show();
            }
            sim->startSimulation();
        }
    }
    if(event->key()==Qt::Key_D){
        if(pos_actual_x<=709){
        pos_actual_x +=10;
        DT->setGeometry(pos_actual_x,pos_actual_y,w,h);
        }
    }
    if(event->key()==Qt::Key_A){
        if(pos_actual_x>=23){
        pos_actual_x -= 10;
        DT->setGeometry(pos_actual_x,pos_actual_y,w,h);
        }

    }
}

void Game::set_lifes(int l){
    ui->Lifes->setNum(l);
}
void Game::set_score(int s){
    ui->Score->setNum(s);
}
void Game::set_diff(int d){
    ui->Dif->setNum(d);
}

