#include "high_score.h"
#include "ui_high_score.h"
#include <string>
#include <QString>

using namespace std;
High_Score::High_Score(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::High_Score),
    window(NULL),
    sim(NULL)
{
    ui->setupUi(this);
    connect(ui->BackHS,SIGNAL(clicked()),this,SLOT(on_back()));
    ui->p1->setNum(0);
    ui->p2->setNum(0);
    ui->p3->setNum(0);
    ui->p4->setNum(0);
    ui->p5->setNum(0);
}

High_Score::~High_Score()
{
    delete ui;
}
void High_Score::set_Things(Widget *window, Simulator *sim){
    this->window=window;
    this->sim = sim;
    set_scores();
}
void High_Score::on_back(){
    window->setVisible(1);
    this->close();
}

void High_Score::set_scores(){
    ui->p1->setNum(sim->scores[0]);
    ui->p2->setNum(sim->scores[1]);
    ui->p3->setNum(sim->scores[2]);
    ui->p4->setNum(sim->scores[3]);
    ui->p5->setNum(sim->scores[4]);
}
