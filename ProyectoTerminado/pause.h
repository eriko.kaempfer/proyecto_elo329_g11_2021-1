#ifndef PAUSE_H
#define PAUSE_H

#include <QWidget>
#include "game.h"
namespace Ui {

class Pause;
}
class Game;
class Widget;
class Simulator;
class Pause : public QWidget
{
    Q_OBJECT

public:
    explicit Pause(QWidget *parent = nullptr);
    ~Pause();
    void set_Things(Game *window = NULL, Widget *mainMenu = NULL,Simulator *sim = NULL);

private:
    Ui::Pause *ui;
    Game *window;
    Widget *mainMenu;
    Simulator *sim;
private slots:
    void on_continue();
    void on_menu();
};

#endif // PAUSE_H
