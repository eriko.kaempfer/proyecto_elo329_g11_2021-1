#include "widget.h"
#include "ui_widget.h"
#include "ui_settings.h"
#include "settings.h"
#include "high_score.h"
#include "game.h"
#include "controls.h"
#include <iostream>

Widget::Widget(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::Widget),
      sim(NULL)
{
    ui->setupUi(this);
    connect(ui->Settings,SIGNAL(clicked()),this,SLOT(showSettings()));
    connect(ui->Exit,SIGNAL(clicked()),this,SLOT(Exit()));
    connect(ui->HS,SIGNAL(clicked()),this,SLOT(showHighScore()));
    connect(ui->Start,SIGNAL(clicked()),this,SLOT(showGame()));
    connect(ui->Controls,SIGNAL(clicked()),this,SLOT(showControls()));

}

Widget::~Widget()
{
    delete ui;
}
void Widget::showSettings(){
    settings *s = new settings();
    s->set_Things(this,sim);
    s->show();
    this->setHidden(1);
}
void Widget::Exit(){
    this->close();
}
void Widget::showHighScore(){
    High_Score *s = new High_Score();
    s->set_Things(this,sim);
    s->show();
    this->setHidden(1);
}
void Widget::showGame(){
    Game *s = new Game();
    s->set_Things(this,sim);
    s->show();
    this->setHidden(1);
}
void Widget::showControls(){
    Controls *s = new Controls();
    s->set_Things(this);
    s->show();
    this->setHidden(1);
}
void Widget::set_Things(Simulator *sim){
    this->sim = sim;
}
