#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "simulator.h"
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE
class Game;
class Simulator;
class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    void set_Things(Simulator *sim);

private:
    Ui::Widget *ui;
    Simulator *sim;

private slots:
    void showSettings();
    void Exit();
    void showHighScore();
    void showGame();
    void showControls();
};
#endif // WIDGET_H
