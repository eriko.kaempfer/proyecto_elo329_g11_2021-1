#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <QObject>
#include <QTimer>
#include <iostream>
#include "game.h"
#include "endgame.h"
class Game;
using namespace std;
class Simulator : public QObject
{
    Q_OBJECT
private:
    QTimer * timer;
    QTimer * timer_balas;
    QTimer * timer_balas_enemigos;
    Game *game;
    int desp = 10;
    int inicio;
public:
    explicit Simulator(QObject *parent = nullptr);
    void startSimulation();
    void set_Game(Game *game);
    void pauseTimer();
    void continueTimer();
    void set_scores();
    bool stateTimer();
    int scores[5]={1100,1000,800,500,0};
    int initial_lifes = 3;
    int lifes = initial_lifes;
    int initial_score = 0;
    int score = initial_score;
    int diff = 1;

signals:
public slots:
    void simulateSlot();
    void simulateSlot_balas();
    void simulateSlot_balas_enemigos();
    void simulateSlot_move_balas();
};

#endif // SIMULATOR_H
