QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    controls.cpp \
    endgame.cpp \
    game.cpp \
    high_score.cpp \
    main.cpp \
    pause.cpp \
    settings.cpp \
    simulator.cpp \
    widget.cpp

HEADERS += \
    controls.h \
    endgame.h \
    game.h \
    high_score.h \
    pause.h \
    settings.h \
    simulator.h \
    widget.h

FORMS += \
    controls.ui \
    endgame.ui \
    game.ui \
    high_score.ui \
    pause.ui \
    settings.ui \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Resources.qrc
DISTFILES += \
        uml_1.qmodel
